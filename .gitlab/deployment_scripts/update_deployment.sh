#!/bin/bash

set -e

HELM_RELEASE_NAME=${HELM_RELEASE_NAME:0:53}
HELM_RELEASE_NAME=$(echo $HELM_RELEASE_NAME | sed 's/-$//')

# Update deployment
helm -n $KUBE_NAMESPACE upgrade ${HELM_RELEASE_NAME} mastodon/ -f .gitlab/production.yml --wait
