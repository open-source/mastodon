#!/bin/bash

set -e

HELM_RELEASE_NAME=${HELM_RELEASE_NAME:0:53}
HELM_RELEASE_NAME=$(echo $HELM_RELEASE_NAME | sed 's/-$//')

# Create/Update the secret
kubectl -n $KUBE_NAMESPACE create secret generic ${HELM_RELEASE_NAME}-config \
  --from-env-file=${PRODUCTION_ENV} \
  --dry-run=client \
  --save-config \
  -o yaml | \
  kubectl apply -f -
