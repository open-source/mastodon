# Upgrades (One minor version at a time)

1. Check [Changelog](https://github.com/mastodon/mastodon/releases) for extra upgrade steps
2. Update dependencies in Dockerfile and change version
3. Update Image version in mastodon/templates/_helpers.tpl and in docker/Dockerfile
4. Push changes and create a tag for the new version. CI will build the new image
5. Fetch current helm values
6. Upgrade helm chart `helm upgrade mastodon ./ -f <values_file>`