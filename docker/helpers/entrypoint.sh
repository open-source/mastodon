#!/bin/bash

case "$1" in
"puma")
    export RAILS_ENV=production;
    export PORT=3000;
    export BIND=0.0.0.0
    bind tcp://$BIND:$PORT
    bundle exec rails assets:precompile
    /usr/local/bundle/bin/bundle exec puma -C config/puma.rb;
    ;;
"streaming")
    export NODE_ENV=production;
    export PORT=4000;
    export STREAMING_CLUSTER_NUM=1;
    /usr/bin/node ./streaming;
    ;;
"sidekiq")
    export RAILS_ENV=production;
    export DB_POOL=25;
    export MALLOC_ARENA_MAX=2;
    /usr/local/bundle/bin/bundle exec sidekiq -c 25
    ;;
*)
  echo "Unknown command... Chose one of [puma|streaming|sidekiq]";
  return 1;
esac